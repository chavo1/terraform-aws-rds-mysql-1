output "arn" {
  value       = aws_db_instance.db.arn
  description = "The ARN of the database instance."
}

output "address" {
  value       = aws_db_instance.db.address
  description = "The hostname of the  database instance."
}

output "port" {
  value       = aws_db_instance.db.port
  description = "The port used."
}

output "identifier" {
  value       = local.identifier
  description = "The database instance identifier."
}

output "name" {
  value       = aws_db_instance.db.name
  description = "The database instance name."
}

output "auth" {
  value       = aws_secretsmanager_secret.auth.arn
  description = "The database AWS Secrets Manager ARN."
}

output "replica_arn" {
  value       = var.read_replica ? aws_db_instance.replica[0].arn : ""
  description = "The ARN of the replica database instance."
}

output "replica_address" {
  value       = var.read_replica ? aws_db_instance.replica[0].address : ""
  description = "The hostname of the replica database instance."
}

output "replica_port" {
  value       = var.read_replica ? aws_db_instance.replica[0].port : ""
  description = "The port used for the replica."
}

output "replica_identifier" {
  value       = var.read_replica ? local.replica_identifier : ""
  description = "The replica database identifier."
}

output "replica_name" {
  value       = var.read_replica ? aws_db_instance.replica[0].name : ""
  description = "The replica database name."
}

