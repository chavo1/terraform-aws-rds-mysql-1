# Tests using the Valhalla account
variable aws_role_arn {
  type = string
}

provider "aws" {
  assume_role {
    role_arn = var.aws_role_arn
  }
  region = "eu-central-1"
}

locals {
  environment = "valhalla"
}
# Get the Valhalla network information
data aws_vpc "valhalla" {
  tags = {
    Environment = "valhalla"
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = data.aws_vpc.valhalla.id

  tags = {
    Tier = "private"
  }
}

data "aws_subnet_ids" "public" {
  vpc_id = data.aws_vpc.valhalla.id

  tags = {
    Tier = "public"
  }
}

module "test_1" {
  source = "./.."

  environment        = "valhalla"
  vpc_id             = data.aws_vpc.valhalla.id
  private_subnet_ids = data.aws_subnet_ids.private.ids
  db_name            = "mysqldb"
  db_identifier      = "mysql-test-1"
  read_replica       = true

  tags = {
    "CostCentre" = "development"
    "Project"    = "pr/001"
  }
}

output "test_1_arn" {
  value = module.test_1.arn
}

output "test_1_address" {
  value = module.test_1.address
}

output "test_1_port" {
  value = module.test_1.port
}

output "test_1_identifier" {
  value = module.test_1.identifier
}

output "test_1_name" {
  value = module.test_1.name
}

output "test_1_auth" {
  value = module.test_1.auth
}

output "test_1_replica_arn" {
  value = module.test_1.replica_arn
}

output "test_1_replica_address" {
  value = module.test_1.replica_address
}

output "test_1_replica_port" {
  value = module.test_1.replica_port
}

output "test_1_replica_identifier" {
  value = module.test_1.replica_identifier
}

output "test_1_replica_name" {
  value = module.test_1.replica_name
}

module "test_2" {
  source = "./.."

  environment        = "valhalla"
  vpc_id             = data.aws_vpc.valhalla.id
  private_subnet_ids = data.aws_subnet_ids.private.ids
  db_name            = "mysqldb2"
  username           = "devopsuser"
  db_identifier      = "mysql-test-2"
  read_replica       = false

  tags = {
    "CostCentre" = "development"
    "Project"    = "pr/001"
  }
}

output "test_2_arn" {
  value = module.test_2.arn
}

output "test_2_address" {
  value = module.test_2.address
}

output "test_2_port" {
  value = module.test_2.port
}

output "test_2_identifier" {
  value = module.test_2.identifier
}

output "test_2_name" {
  value = module.test_2.name
}

output "test_2_auth" {
  value = module.test_2.auth
}

output "test_2_replica_arn" {
  value = module.test_2.replica_arn
}

output "test_2_replica_address" {
  value = module.test_2.replica_address
}

output "test_2_replica_port" {
  value = module.test_2.replica_port
}

output "test_2_replica_identifier" {
  value = module.test_2.replica_identifier
}

output "test_2_replica_name" {
  value = module.test_2.replica_name
}
